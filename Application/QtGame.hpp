#pragma once

#include "QtPresentation.h"
#include "QtGrid.hpp"
#include "CStdRandomizer.hpp"
#include "Grid.hpp"
#include <optional>

class QtGame
{
public:
    QtGame(QtPresentation& presentation,
           Size rows, Size columns,
           const QtGrid::Colors& colors);
    ~QtGame();

    void makeCity();
    void addAircraft(Qt::Key bomb = Qt::Key_Down, Qt::Key rocket = Qt::Key_Left);


private:
    void reset();
    void move();
    void speedUp();
    void speedDown();
    void flipTimer();
    void setGround();
    void restartScreen();

    QtPresentation& presentation;
    Grid grid;
    QtGrid qtGrid;
    CStdRandomizer randomizer;
    Size rows;
    Size columns;
    const int initialDuration;
    int moveDuration;
    std::optional<int> moveTimer = std::nullopt;
};

