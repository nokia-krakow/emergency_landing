#include "QtGame.hpp"
#include <algorithm>
#include <iostream>

QtGame::QtGame(QtPresentation &presentation,
               Size rows, Size columns,
               const QtGrid::Colors &colors)
    : presentation(presentation),
      grid(rows, columns),
      qtGrid(presentation, grid, colors),
      randomizer(),
      rows(rows),
      columns(columns),
      initialDuration(100),
      moveDuration(initialDuration)
{
    setGround();
    presentation.setKeyListener(Qt::Key_Escape, [this] { reset(); });
    presentation.setKeyListener(Qt::Key_PageUp, [this] { speedUp(); });
    presentation.setKeyListener(Qt::Key_PageDown, [this] { speedDown(); });
    presentation.setKeyListener(Qt::Key_Return, [this] { flipTimer(); });
    presentation.setKeyListener(Qt::Key_Pause, [this] { flipTimer(); });
}

QtGame::~QtGame()
{
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
    }
}

void QtGame::makeCity()
{
    // TODO
}

void QtGame::addAircraft(Qt::Key bomb, Qt::Key rocket)
{
    //  #  #
    // #####
    Path aircraftBody{{columns - 5, 1},
                      {columns - 4, 1}, {columns - 4, 0},
                      {columns - 3, 1},
                      {columns - 2, 1},
                      {columns - 1, 1}, {columns - 1, 0}};
    presentation.setKeyListener(bomb, [this/*, &aircraft*/]
    {
        // TODO
    });
    presentation.setKeyListener(rocket, [this/*, &aircraft*/]
    {
        // TODO
    });
}

void QtGame::reset()
{
    // TODO - something might be missing here
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
        moveTimer = std::nullopt;
    }
    restartScreen();
    makeCity();
    addAircraft();
}

void QtGame::move()
{
    // TODO
}

void QtGame::speedUp()
{
    int newDuration = moveDuration * 2;
    if (newDuration > 1000)
        newDuration = 1000;
    if (newDuration != moveDuration)
    {
        moveDuration = newDuration;
        if (moveTimer)
        {
            presentation.stopTimer(*moveTimer);
            moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
        }
    }
}

void QtGame::speedDown()
{
    int newDuration = moveDuration / 2;
    if (newDuration < 10)
        newDuration = 10;
    if (newDuration != moveDuration)
    {
        moveDuration = newDuration;
        if (moveTimer)
        {
            presentation.stopTimer(*moveTimer);
            moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
        }
    }
}

void QtGame::flipTimer()
{
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
        moveTimer = std::nullopt;
    }
    else
    {
        moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
    }
}

void QtGame::setGround()
{
    for (Position c = 0; c < columns; ++c)
    {
        qtGrid.setCell(Position2D(c, rows - 1), IGrid::Cell::ground);
    }
}

void QtGame::restartScreen()
{
    for (Position r = 0; r < rows - 1; ++r)
    {
        for (Position c = 0; c < columns; ++c)
        {
            qtGrid.setCell(Position2D(c, r), IGrid::Cell::sky);
        }
    }
}
