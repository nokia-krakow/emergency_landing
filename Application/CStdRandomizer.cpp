#include "CStdRandomizer.hpp"
#include <ctime>


CStdRandomizer::CStdRandomizer()
    : randomEngine(std::time(0)) // used time(0) - because std::random_device does not work
{
}

Position CStdRandomizer::rand(Size elements)
{
    if (elements == 0)
    {
        return 0;
    }
    Distribution distribution{0, static_cast<Size>(elements - 1)};
    return distribution(randomEngine);
}

