#pragma once

#include <cstdint>
#include <tuple>
#include <deque>

using Position = std::uint16_t;
using Position2D = std::tuple<Position,Position>;
using Size = Position;
using Size2D = Position2D;
using Path = std::deque<Position2D>;

inline Position& x(Position2D& p) { return std::get<0>(p); }
inline Position const& x(Position2D const& p) { return std::get<0>(p); }
inline Position& y(Position2D& p) { return std::get<1>(p); }
inline Position const& y(Position2D const& p) { return std::get<1>(p); }
