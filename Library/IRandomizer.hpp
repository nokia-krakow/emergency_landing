#pragma once

#include "Types.hpp"

class IRandomizer
{
public:
    virtual ~IRandomizer() = default;
    virtual Position rand(Size elements) = 0;
};

