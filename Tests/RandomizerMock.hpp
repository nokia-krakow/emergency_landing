#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "IRandomizer.hpp"
class RandomizerMock : public IRandomizer
{
public:
    RandomizerMock();
    ~RandomizerMock() override;
    MOCK_METHOD1(rand, Position(Size));
};
